//
//  CollectionPresenter.swift
//  MarketPlace-SwiftBook
//
//  Created by Sergei Vikhliaev on 05.03.2023.
//

import Foundation

protocol CollectionPresentation {
  func didSelectCategory(with id: Int)
}

final class CollectionPresenter {

  var router: CollectionRouting?

}

extension CollectionPresenter: CollectionPresentation {
  func didSelectCategory(with id: Int) {
    router?.routeTo(target: CollectionRouter.Targets.categoryWithProducts(id: id))
  }
}
