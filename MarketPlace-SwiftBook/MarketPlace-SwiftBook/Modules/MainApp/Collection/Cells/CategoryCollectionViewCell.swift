//
//  CategoryCollectionViewCell.swift
//  MarketPlace-SwiftBook
//
//  Created by Sergei Vikhliaev on 05.03.2023.
//

import UIKit

final class CategoryCollectionViewCell: UICollectionViewCell {

  var viewModel: CategoryViewModel?

  func configure(with viewModel: CategoryViewModel) {
    self.viewModel = viewModel
  }
}
