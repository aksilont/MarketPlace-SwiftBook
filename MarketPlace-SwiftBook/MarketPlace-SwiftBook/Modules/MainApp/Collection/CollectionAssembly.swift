//
//  CollectionAssembly.swift
//  MarketPlace-SwiftBook
//
//  Created by Sergei Vikhliaev on 05.03.2023.
//

import Foundation
import UIKit

final class CollectionAssembly {

  private let navigaionController: UINavigationController

  init(navigaionController: UINavigationController) {
    self.navigaionController = navigaionController
  }
}

extension CollectionAssembly: BaseAssembly {
  func configure(viewController: UIViewController) {
    guard let registrationScreenViewController = viewController as? CollectionViewController else { return }

    let presenter = CollectionPresenter()
    let router = CollectionRouter(navigationContoller: navigaionController)

    registrationScreenViewController.presenter = presenter
    presenter.router = router
  }
}
