//
//  TabBarViewController.swift
//  MarketPlace-SwiftBook
//
//  Created by Ilya on 25.01.23.
//

import UIKit

/// ТабБар приложения и сборка экранов
final class TabBarViewController: UITabBarController {
  /// Перечесление со всеми экранами, для фунции switchTo
  enum Tabs: Int {
    case profile
    case basket
    case collection
  }

  override func viewDidLoad() {
    super.viewDidLoad()

    configureUI()
    switchTo(tab: .profile)
  }
}

private extension TabBarViewController {
  /// При переходе на табБар будет открыватся выбранная страница
  /// ( потом можно протянуть в настройках приложения какую страницу открывать по умолчанию )
  /// - Parameter tab: выбор экрана ( ограниченный выбор, только из существующих )
  func switchTo(tab: Tabs) {
    selectedIndex = tab.rawValue
  }

  /// Настройка ТабБара
  func configureUI() {
    buildViewControllers()

    tabBar.backgroundColor = .systemBackground
    tabBar.layer.masksToBounds = true
  }

  /// Настройка экрана и его таба
  /// - Parameters:
  ///   - controller: Для какого экрана
  ///   - icon: Иконка по умолчанию
  ///   - selectIcon: Иконка при выборе экрана
  ///   - title: Название таба
  /// - Returns: Возвращает собранный экран
  func configureViewControllerAsTab(
    controller: UIViewController,
    icon: UIImage?,
    selectIcon: UIImage?,
    title: String) -> UINavigationController {
      let tab = UINavigationController(rootViewController: controller)

      tab.tabBarItem.image = icon
      tab.tabBarItem.selectedImage = selectIcon
      tab.tabBarItem.title = title
      tab.isNavigationBarHidden = true
      return tab
    }

  /// Билдер экранов приложения
  func buildViewControllers() {
    let profileVC = configureViewControllerAsTab(
      controller: ProfileViewController(),
      icon: UIImage(named: "user"),
      selectIcon: UIImage(named: "userSelect"),
      title: "profile")
    let basketVC = BasketViewController()
    let basketNavigationVC = configureViewControllerAsTab(
      controller: BasketViewController(),
      icon: UIImage(named: "basket"),
      selectIcon: UIImage(named: "basketSelect"),
      title: "basket")
    let basketAssembly = BasketAssembly(navigaionController: basketNavigationVC)
    basketAssembly.configure(viewController: basketVC)

    let collectionVC = CollectionViewController()
    let collectionNavigationVC = configureViewControllerAsTab(
      controller: collectionVC,
      icon: UIImage(named: "collection"),
      selectIcon: UIImage(named: "collectionSelect"),
      title: "collection")
    let collectionAssembly = CollectionAssembly(navigaionController: collectionNavigationVC)
    collectionAssembly.configure(viewController: collectionVC)

    setViewControllers([
      collectionNavigationVC,
      basketNavigationVC,
      profileVC
    ], animated: false)
  }
}
