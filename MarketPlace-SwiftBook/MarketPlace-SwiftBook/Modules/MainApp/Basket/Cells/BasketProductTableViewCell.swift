//
//  BasketProductTableViewCell.swift
//  MarketPlace-SwiftBook
//
//  Created by Sergei Vikhliaev on 02.04.2023.
//

import UIKit

final class BasketProductTableViewCell: UITableViewCell {
  @IBOutlet weak var productImageView: UIImageView!
  @IBOutlet weak var productNameLabel: UILabel!
  @IBOutlet weak var productCostLabel: UILabel!
  @IBOutlet weak var countOfThisProductLabel: UILabel!
  @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
  @IBAction func removeOneProductFromBasket(_ sender: Any) {
  }
  @IBAction func addOneProductToBasket(_ sender: Any) {
  }

  var productInBasketViewModel: ProductInBasketViewModel? {
    didSet {
      configure(with: productInBasketViewModel)
    }
  }

  override func prepareForReuse() {
    super.prepareForReuse()
    productInBasketViewModel = nil
  }
}

extension BasketProductTableViewCell {
  func configure(with viewModel: ProductInBasketViewModel?) {
    if let url = viewModel?.imageSourceURL {
      downloadImage(with: url, to: productImageView)
    }
    productNameLabel.text = viewModel?.productName
    productCostLabel.text = viewModel?.productCost
    countOfThisProductLabel.text = viewModel?.countOfThisProduct
  }
}

private extension BasketProductTableViewCell {
  func downloadImage(with url: URL, to imageView: UIImageView) {
    DispatchQueue.global(qos: .utility).async { [weak self] in
      URLSession.shared.dataTask(with: url, completionHandler: { [weak self] (data, responce, error) in
        var action: (()->())?

        if let data = data {
          let image = UIImage(data: data)
          action = {
            imageView.image = image
            self?.activityIndicator.stopAnimating()
          }
        } else if let error = error {
          action = {
            print(error.localizedDescription)
            imageView.image = UIImage.init(systemName: "questionmark.app.fill")
            self?.activityIndicator.stopAnimating()
          }
        } else {
          action = {
            print("Unknoun error")
            self?.activityIndicator.stopAnimating()
          }
        }

        DispatchQueue.main.async {
          action?()
        }
      }).resume()
    }
  }
}



