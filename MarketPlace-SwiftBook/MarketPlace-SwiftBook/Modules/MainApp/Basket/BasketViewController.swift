//
//  BasketViewController.swift
//  MarketPlace-SwiftBook
//
//  Created by Sergei Vikhliaev on 02.04.2023.
//

import UIKit

final class BasketViewController: UIViewController {

  @IBOutlet weak var tableView: UITableView!

  let viewModels = [
    ProductInBasketViewModel(id: 0,
                             productName: "Coffe",
                             productCost: "100",
                             imageSourceURL: URL(string: "http://s4.fotokto.ru/photo/full/340/3408962.jpg"),
                             countOfThisProduct: "2"),
    ProductInBasketViewModel(id: 1,
                             productName: "watches",
                             productCost: "10 000",
                             imageSourceURL: URL(string: "https://i0.pngocean.com/files/708/902/522/watch-bon-air-motel-ltd-clip-art-watch-png.jpg"),
                             countOfThisProduct: "2")
  ]

  var presenter: BasketPresentation?

  override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
    super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
  }

  required init?(coder: NSCoder) {
    super.init(coder: coder)
  }

  override func viewDidLoad() {
    super.viewDidLoad()
    registerXibs()
    setupTableView()
    tableView.reloadData()
  }
}

private extension BasketViewController {
  func registerXibs() {
    let xibNames = [
      String(describing: BasketProductTableViewCell.self)
    ]

    xibNames.forEach { (name) in
      tableView.register(UINib(nibName: name, bundle: nil), forCellReuseIdentifier: name)
    }
  }

  func setupTableView() {
    tableView.delegate = self
    tableView.dataSource = self
  }
}

extension BasketViewController: UITableViewDelegate {}

extension BasketViewController: UITableViewDataSource {

  func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
    150
  }

  func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    viewModels.count
  }

  func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    let cell = tableView
      .dequeueReusableCell(withIdentifier: String(describing: BasketProductTableViewCell.self),
                           for: indexPath) as! BasketProductTableViewCell
    cell.configure(with: viewModels[indexPath.row])
    return cell
  }
}
