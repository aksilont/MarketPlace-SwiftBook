//
//  LoginScreenViewController.swift
//  MarketPlace-SwiftBook
//
//  Created by Dmitry Danilin on 25.12.2022.
//

import UIKit

/// Экран логина для входа в проект
final class LoginScreenViewController: UIViewController {

    var presenter: LoginPresentation?
    
    // MARK: - IBOutlets
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var loginTextField: UITextField!
    @IBOutlet weak var registrationButton: UIButton!
    
    // MARK: - LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setup()
    }
    
    // MARK: - Override Methods
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        view.endEditing(true)
    }
    
    // MARK: - IBActions
    @IBAction func registrationPressedButton() {
      presenter?.runRegistratonFlow()
    }

    @IBAction func enterPressedButton() {
        (UIApplication.shared.delegate as? AppDelegate)?.changeRootViewController(TabBarViewController())
    }
}

// MARK: - Configuration ViewController
private extension LoginScreenViewController {
    
    func setup() {
        setupTextFields()
        setupButtons()
    }
    
    func setupTextFields() {
        loginTextField.delegate = self
        passwordTextField.delegate = self
    }
    
    func setupButtons() {
        registrationButton.layer.borderWidth = 1
        registrationButton.layer.cornerRadius = 8
    }
}

// MARK: - Text Field Delegate
extension LoginScreenViewController: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
}
