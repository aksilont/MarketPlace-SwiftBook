//
//  ManagerAssembly.swift
//  MarketPlace-SwiftBook
//
//  Created by Дмитрий Данилин on 18.01.2023.
//

import Foundation

/// Класс для сборки зависимостей в менеджеры
final class ManagerAssembly {
    private let service = ServiceAssembly()
    
    /// Менеджер для проверки токеноа
    lazy var tokenCheckerManager: ITokenCheckerManagement = {
        return TokenCheckerManager(keychainStorageService: service.keychainStorageService)
    }()
}
